/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "SynthVoice.h"
#include "SynthSound.h"


//==============================================================================
/**
*/
class GeoHeadalphaAudioProcessor  : public AudioProcessor,
                                    private AudioProcessorValueTreeState::Listener
{
public:
    //==============================================================================
    GeoHeadalphaAudioProcessor();
    ~GeoHeadalphaAudioProcessor();

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (AudioBuffer<float>&, MidiBuffer&) override;

    //==============================================================================
    AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const String getProgramName (int index) override;
    void changeProgramName (int index, const String& newName) override;

    //==============================================================================
    void getStateInformation (MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;

    //==============================================================================
    AudioProcessorValueTreeState parameters;

    //==============================================================================
    void createWavetableCardioid(std::atomic<float>* mode, std::atomic<float>* points,
                  std::atomic<float>* factor, std::atomic<float>* course );

    void createWavetableDraw();

    void setParameters();

    //=============================================================================
    void createWavetableTemplates();

    //=============================================================================
    void updateFilter();

    //=============================================================================
    float waveform[1080] = {0};
    float waveform_holder[1080] = {0};

private:
    void parameterChanged(const String &parameterID, float newValue);

    //==============================================================================
    std::atomic<float>* modeParameter = nullptr;
    std::atomic<float>* pointParameter = nullptr;
    std::atomic<float>* factorParameter = nullptr;
    std::atomic<float>* courseParameter = nullptr;
    std::atomic<float>* measureParameter = nullptr;

    //==============================================================================
    std::atomic<float>* attackParameter = nullptr;
    std::atomic<float>* decayParameter = nullptr;
    std::atomic<float>* sustainParameter = nullptr;
    std::atomic<float>* releaseParameter = nullptr;

    //==============================================================================
    std::atomic<float>* buttonLeftParameter = nullptr;
    std::atomic<float>* buttonRightParameter = nullptr;

    //==============================================================================
    int vec_pos; // postCardioidWaveform

    //==============================================================================
    float t, t_2, x, y; // createWavetableCardioid
    float radian_x, radian_y, radian_x_2, radian_y_2;
    float rad_wav, pos_wav;
    float chord_lenght;

    //==============================================================================
    Synthesiser mySynth;
    SynthVoice* myVoice;

    double lastSampleRate;

    //==============================================================================
    int algorithm = 1;

    //==============================================================================
    float waveform_templates[4][1080] = { {0}, {0}, {0}, {0}};

    //==============================================================================
    dsp::ProcessorDuplicator<dsp::StateVariableFilter::Filter<float> , dsp::StateVariableFilter::Parameters<float>> stateVariableFilter;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (GeoHeadalphaAudioProcessor)

};
