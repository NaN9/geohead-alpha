/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"
#include "CircleCardioid.h"
#include "BoxWavetable.h"
#include "FilterMenu.h"

//==============================================================================
/**
*/
class GeoHeadalphaAudioProcessorEditor  : public AudioProcessorEditor,
                                          public Slider::Listener,
                                          public Button::Listener
{
public:
    GeoHeadalphaAudioProcessorEditor (GeoHeadalphaAudioProcessor&);
    ~GeoHeadalphaAudioProcessorEditor();

    //==========================================================================
    void paint (Graphics&) override;
    void resized() override;

    //==========================================================================
    void modeSelektorChanged();
    void sliderValueChanged (Slider *slider);
    void buttonClicked (Button *button);

    //==========================================================================
    void copyWaveform();

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    GeoHeadalphaAudioProcessor& processor;

    //==========================================================================
    ComboBox modeSelektor;
    int mode;

    //==========================================================================
    Slider pointSlider, factorSlider, courseSlider, measureSlider;

    //==========================================================================
    Slider attackSlider, decaySlider, sustainSlider, releaseSlider;

    //==========================================================================
    ArrowButton button_left {"Button_Right", 0.5f, Colours::black };
    ArrowButton button_right {"Button_left", 0.0f, Colours::black };

    //==========================================================================
    CircleCardioid cardioidComponent;
    BoxWavetable wavetableComponent;

    FilterMenu filterComponent {processor};

    //==========================================================================
    int algorithm = 1;

public:
    std::unique_ptr <AudioProcessorValueTreeState::ComboBoxAttachment> modeAttachment;

    std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> pointAttachment;
    std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> factorAttachment;
    std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> courseAttachment;
    std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> measureAttachment;

    std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> attackAttachment;
    std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> decayAttachment;
    std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> sustainAttachment;
    std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> releaseAttachment;

    std::unique_ptr <AudioProcessorValueTreeState::ButtonAttachment> buttonLAttachment;
    std::unique_ptr <AudioProcessorValueTreeState::ButtonAttachment> buttonRAttachment;


    //==========================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (GeoHeadalphaAudioProcessorEditor)
};
