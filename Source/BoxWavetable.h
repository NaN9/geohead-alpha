/*
  ==============================================================================

    BoxWavetable.h
    Created: 1 Jul 2020 12:52:40pm
    Author:  eye

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>


//==============================================================================
/*
*/
class BoxWavetable    : public Component
{
public:
    BoxWavetable()
    {
        // In your constructor, you should add any child components, and
        // initialise any special settings that your component needs.

    }

    ~BoxWavetable()
    {
    }

    void paint (Graphics& g) override
    {
        /* This demo code just fills the component's background and
           draws some placeholder text to get you started.

           You should replace everything in this method with your own
           drawing code..
        */

        g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));   // clear the background

        g.setColour (Colours::black);
        g.drawRect (getLocalBounds(), 3);   // draw an outline around the component
        g.drawLine (0, getHeight()/2, getWidth(), getHeight()/2);
        y_middle = getHeight()/2;

        Path waveform_path;
        x_path_waveform= 3;
        y_path_waveform = y_middle;

        waveform_path.startNewSubPath (x_path_waveform, y_path_waveform);

        for (int i = 0; i < 1079; i++)
        {
          y_path_waveform = static_cast<int>(round (y_middle - waveform[i] * 100));
          x_path_waveform = 3 + static_cast<int>(round (i * 555 / 1080));

          waveform_path.lineTo(x_path_waveform, y_path_waveform);
        }
        g.strokePath (waveform_path, PathStrokeType (2.1f));

    }

    void resized() override
    {
        // This method is where you should set the bounds of any child
        // components that your component contains..

    }

private:

    //==========================================================================

    int y_middle;
    int x_path_waveform;
    int y_path_waveform;

    //==========================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (BoxWavetable)

public:
    float waveform[1080] = {0};

};
