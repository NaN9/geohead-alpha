/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
GeoHeadalphaAudioProcessor::GeoHeadalphaAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", AudioChannelSet::stereo(), true)
                     #endif
                   ),
               parameters (*this, nullptr, Identifier ("PARAMETERS"),
                 {
                    std::make_unique<AudioParameterInt> (  "MODE_ID", "Mode",
                                                            1,              // minimum value
                                                            7,              // maximum value
                                                            1),             // default value
                    std::make_unique<AudioParameterInt>("POINT_ID", "Points", 0, 777, 10),
                    std::make_unique<AudioParameterInt>("FACTOR_ID", "Factor", 0, 23, 2),
                    std::make_unique<AudioParameterFloat>("COURSE_ID", "Course", NormalisableRange<float> (0.000f, 1.0f), 0),
                    std::make_unique<AudioParameterInt>("MEASURE_ID", "Measure", 0, 144, 11),
                    std::make_unique<AudioParameterFloat>("ATTACK_ID", "Attack", NormalisableRange<float> (0.01f, 5.0f), 0.01),
                    std::make_unique<AudioParameterFloat>("DECAY_ID", "Decay", NormalisableRange<float> (0.01f, 2.0f), 0.01),
                    std::make_unique<AudioParameterFloat>("SUSTAIN_ID", "Sustain", NormalisableRange<float> (0.0f, 1.0f), 1),
                    std::make_unique<AudioParameterFloat>("RELEASE_ID", "Release", NormalisableRange<float> (0.01f, 5.0f), 0),
                    std::make_unique<AudioParameterBool> ("BUTTON_L_ID", "Left button", false),
                    std::make_unique<AudioParameterBool> ("BUTTON_R_ID", "Right button", false),
                    std::make_unique<AudioParameterFloat>("FILTER_TYPE_ID", "Filter type", NormalisableRange<float>(0.0f, 2.0f), 0.0f),
                    std::make_unique<AudioParameterFloat>("CUTOFF_ID", "Filter cutoff", NormalisableRange<float>(20.0f, 10000.0f), 400.0f),
                    std::make_unique<AudioParameterFloat>("RESONANCE_ID", "Filter resonance", NormalisableRange<float>(1.0f, 5.0f), 1.0f),
                 })
#endif
{
  parameters.addParameterListener("MODE_ID", this);
  parameters.addParameterListener("POINT_ID", this);
  parameters.addParameterListener("FACTOR_ID", this);
  parameters.addParameterListener("COURSE_ID", this);
  parameters.addParameterListener("MEASURE_ID", this);

  parameters.addParameterListener("ATTACK_ID", this);
  parameters.addParameterListener("DECAY_ID", this);
  parameters.addParameterListener("SUSTAIN_ID", this);
  parameters.addParameterListener("RELEASE_ID", this);

  parameters.addParameterListener("BUTTON_L_ID", this);
  parameters.addParameterListener("BUTTON_R_ID", this);

  mySynth.clearVoices();

  for (int i = 0; i < 5; i++)
  {
      mySynth.addVoice(new SynthVoice());
  }
  mySynth.clearSounds();
  mySynth.addSound(new SynthSound());
}

GeoHeadalphaAudioProcessor::~GeoHeadalphaAudioProcessor()
{
}

//==============================================================================
const String GeoHeadalphaAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool GeoHeadalphaAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool GeoHeadalphaAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool GeoHeadalphaAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double GeoHeadalphaAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int GeoHeadalphaAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int GeoHeadalphaAudioProcessor::getCurrentProgram()
{
    return 0;
}

void GeoHeadalphaAudioProcessor::setCurrentProgram (int index)
{
}

const String GeoHeadalphaAudioProcessor::getProgramName (int index)
{
    return {};
}

void GeoHeadalphaAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void GeoHeadalphaAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
  ignoreUnused(samplesPerBlock);
  lastSampleRate = sampleRate;
  mySynth.setCurrentPlaybackSampleRate(lastSampleRate);

  createWavetableTemplates();
  setParameters();

  if (*modeParameter == 1.0f || 2.0f || 3.0f)
  {
    createWavetableCardioid(modeParameter, pointParameter, factorParameter, courseParameter);
  } else
  {
    createWavetableDraw();
  }

  dsp::ProcessSpec spec;
  spec.sampleRate = lastSampleRate;
  spec.maximumBlockSize = samplesPerBlock;
  spec.numChannels = getTotalNumOutputChannels();

  stateVariableFilter.reset();
  stateVariableFilter.prepare(spec);
  updateFilter();

}

void GeoHeadalphaAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool GeoHeadalphaAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if (layouts.getMainOutputChannelSet() != AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void GeoHeadalphaAudioProcessor::processBlock (AudioBuffer<float>& buffer, MidiBuffer& midiMessages)
{
    ScopedNoDenormals noDenormals;
    //auto totalNumInputChannels  = getTotalNumInputChannels();
    //auto totalNumOutputChannels = getTotalNumOutputChannels();

    for (int i = 0; i < mySynth.getNumVoices(); i++)
    {
        //if myVoice sucessfully casts as a SynthVoice*, get the voice and set the params
        if ((myVoice = dynamic_cast<SynthVoice*>(mySynth.getVoice(i))))
        {
          myVoice->setSampleRate(lastSampleRate);
          myVoice->setWavetable(waveform);
          myVoice->setADSRParameters(attackParameter, decayParameter, sustainParameter, releaseParameter);
        }
    }

    buffer.clear();
    mySynth.renderNextBlock(buffer, midiMessages, 0, buffer.getNumSamples());
    updateFilter();
    dsp::AudioBlock<float> block (buffer);
    stateVariableFilter.process(dsp::ProcessContextReplacing<float> (block));
}

//==============================================================================
bool GeoHeadalphaAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* GeoHeadalphaAudioProcessor::createEditor()
{
    return new GeoHeadalphaAudioProcessorEditor (*this);
}

//==============================================================================
void GeoHeadalphaAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void GeoHeadalphaAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

void GeoHeadalphaAudioProcessor::parameterChanged(const String & parameterID, float newValue)
{
  setParameters();

  if(parameterID == "BUTTON_L_ID")
  {
    if(algorithm < 2)
    {
      algorithm = 3;
    } else
    {
      algorithm--;
    }
  }
  
  if(parameterID == "BUTTON_R_ID")
  {
    if(algorithm > 2)
    {
      algorithm = 1;
    } else
    {
      algorithm++;
    }
  }
  createWavetableCardioid(modeParameter, pointParameter, factorParameter, courseParameter);
}
//==============================================================================
void GeoHeadalphaAudioProcessor::createWavetableCardioid(std::atomic<float>* mode, std::atomic<float>* points,
              std::atomic<float>* factor, std::atomic<float>* course )
{
  /* It calculates the distance from the origin of the circle to where the line
   is drawn. At x points on every line. Then it calculates the average of the
   points while it's putting them in the wavetable array at the correct location
   maximum depth is 1/3 of a degree  */
  chord_lenght = 2 * sin( (MathConstants<float>::twoPi / *points / 2));
  int waveform_selection = static_cast<int>(*mode);

  int points_int = static_cast<int> (*points);
  for (int i=0; i < points_int; i++)
  {
    t = MathConstants<float>::twoPi * (i / *points);
    t_2 =  t * (*factor + *course);

    radian_x = cos(t);
    radian_y = sin(t);
    radian_x_2 = cos(t_2);
    radian_y_2 = sin(t_2);

    float increment = static_cast<int> (*measureParameter);
    increment = 1 / increment;
    for(float j = 0.0f; j <= 1; j += increment)
    {
      x = j * radian_x + (1 - j) * radian_x_2;
      y = j * radian_y + (1 - j) * radian_y_2;
      rad_wav = std::sqrt(x * x + y * y);
      pos_wav = t + chord_lenght * j;

      while(pos_wav > MathConstants<float>::twoPi)
      {
         pos_wav -= MathConstants<float>::twoPi;
      }

      vec_pos = static_cast<int>( round ( pos_wav * (540 / (MathConstants<float>::pi))));

      rad_wav = rad_wav * waveform_templates[waveform_selection][vec_pos];

      if(waveform_holder[vec_pos] != 0.0f)
      {
        float total = (waveform_holder[vec_pos] + rad_wav) / 2;
        waveform_holder[vec_pos] = total;
      }
      else
      {
        waveform_holder[vec_pos] = rad_wav;
      }
    }
  }

  switch(algorithm)
  {
    case 1:
      {
        for(int i = 0; i < 1079; i++)
        {
          if(waveform_holder[i] != 0)
          {
            waveform[i] = waveform_holder[i];
          } else
          {
            waveform[i] = waveform_templates[waveform_selection][i];
          }
        }
        break;
      }
      case 2:
        {
          for(int i = 0; i < 1079; i++)
          {
            waveform[i] = waveform_holder[i];
          }
          break;
        }
      case 3:
        {
          for(int i = 0; i < 1079; i++)
          {
            if(waveform_holder[i] != 0)
            {
              waveform[i] = waveform_holder[i];
            } else
            {
              if(i == 0){
                waveform[i] = 0;
              } else
              {
                waveform[i] = waveform[i - 1];
              }
            }
          }
          break;
        }
  }

  std::fill_n(waveform_holder, 1080, 0);
}

void GeoHeadalphaAudioProcessor::createWavetableDraw()
{
  std::cout << "draw" << std::endl;
}

void GeoHeadalphaAudioProcessor::setParameters()
{
  modeParameter = parameters.getRawParameterValue ("MODE_ID");
  pointParameter  = parameters.getRawParameterValue ("POINT_ID");
  factorParameter  = parameters.getRawParameterValue ("FACTOR_ID");
  courseParameter  = parameters.getRawParameterValue ("COURSE_ID");
  measureParameter = parameters.getRawParameterValue("MEASURE_ID");

  attackParameter = parameters.getRawParameterValue ("ATTACK_ID");
  decayParameter = parameters.getRawParameterValue ("DECAY_ID");
  sustainParameter = parameters.getRawParameterValue ("SUSTAIN_ID");
  releaseParameter = parameters.getRawParameterValue ("RELEASE_ID");
}

void GeoHeadalphaAudioProcessor::createWavetableTemplates()
{

  for(int i = 0; i < 1080; i++)
  {
    waveform_templates[1][i] = sin( MathConstants<float>::twoPi * i / 1080);
  }

  for(int i = 0; i < 1080; i++)
  {
    float mid = i / 1080.0f;
    if( mid <= 0.5f)
    {
      waveform_templates[2][i] = 1.0f;
    } else
    {
      waveform_templates[2][i] = -1.0f;
    }
  }

    for(int i = 0; i < 1080; i++)
    {
      waveform_templates[3][i] = (i / 1080.0f) * 2 - 1.0;
    }


  for(int i = 0; i < 1080; i++)
  {

  }

}

void GeoHeadalphaAudioProcessor::updateFilter()
{
  int menuChoice = *parameters.getRawParameterValue("FILTER_TYPE_ID");
  int freq = *parameters.getRawParameterValue("CUTOFF_ID");
  int res = *parameters.getRawParameterValue("RESONANCE_ID");

  if (menuChoice == 0)
  {
      stateVariableFilter.state->type = dsp::StateVariableFilter::Parameters<float>::Type::lowPass;
      stateVariableFilter.state->setCutOffFrequency(lastSampleRate, freq, res);
  }

  if (menuChoice == 1)
  {
      stateVariableFilter.state->type = dsp::StateVariableFilter::Parameters<float>::Type::highPass;
      stateVariableFilter.state->setCutOffFrequency(lastSampleRate, freq, res);
  }

  if (menuChoice == 2)
  {
      stateVariableFilter.state->type = dsp::StateVariableFilter::Parameters<float>::Type::bandPass;
      stateVariableFilter.state->setCutOffFrequency(lastSampleRate, freq, res);
  }
}


//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new GeoHeadalphaAudioProcessor();
}
