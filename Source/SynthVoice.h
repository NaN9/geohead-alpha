/*
  ==============================================================================

    SynthVoice.h
    Created: 6 Jul 2020 6:33:34pm
    Author:  eye

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "SynthSound.h"

class SynthVoice : public SynthesiserVoice
{
public:
    bool canPlaySound (SynthesiserSound* sound) override
    {
        return dynamic_cast <SynthSound*>(sound) != nullptr;
    }

    void setPitchBend(int pitchWheelPos)
    {
        if (pitchWheelPos > 8192)
        {
            // shifting up
            pitchBend = float(pitchWheelPos - 8192) / (16383 - 8192);
        }
        else
        {
            // shifting down
            pitchBend = float(8192 - pitchWheelPos) / -8192;    // negative number
        }
    }

    float pitchBendCents()
    {
        if (pitchBend >= 0.0f)
        {
            // shifting up
            return pitchBend * pitchBendUpSemitones * 100;
        }
        else
        {
            // shifting down
            return pitchBend * pitchBendDownSemitones * 100;
        }
    }

    static double noteHz(int midiNoteNumber, double centsOffset)
    {
        double hertz = MidiMessage::getMidiNoteInHertz(midiNoteNumber);
        hertz *= std::pow(2.0, centsOffset / 1200);
        return hertz;
    }

    //=======================================================

    void startNote (int midiNoteNumber, float velocity, SynthesiserSound* sound, int currentPitchWheelPosition) override
    {
        adsr.noteOn();
        noteNumber = midiNoteNumber;
        setPitchBend(currentPitchWheelPosition);
        frequency = noteHz(noteNumber, pitchBendCents());
        level = velocity;
        phase_0 = 0.0f;
    }

    //=======================================================

    void stopNote (float velocity, bool allowTailOff) override
    {
      adsr.noteOff();
        if (velocity == 0)
            clearCurrentNote();
    }

    //=======================================================

    void pitchWheelMoved (int newPitchWheelValue) override
    {
        setPitchBend(newPitchWheelValue);
        frequency = noteHz(noteNumber, pitchBendCents());
    }

    //=======================================================

    void controllerMoved (int controllerNumber, int newControllerValue) override
    {

    }

    void setSampleRate(double lastSampleRate)
    {
      samplerate = lastSampleRate;
      adsr.setSampleRate(samplerate);
    }

    void setWavetable(float* waveform)
    {
      wavetable = waveform;
    }

    void setADSRParameters (std::atomic<float>* attack, std::atomic<float>* decay, std::atomic<float>* sustain, std::atomic<float>* release)
    {
      adsr_parameters.attack = *attack;
      adsr_parameters.decay = *decay;
      adsr_parameters.sustain = *sustain;
      adsr_parameters.release = *release;
    }

    void renderNextBlock (AudioBuffer <float> &outputBuffer, int startSample, int numSamples) override
    {
      increment = frequency * 1080 / samplerate;
      adsr.setParameters(adsr_parameters);
        for (int sample = 0; sample < numSamples; ++sample)
        {
            for (int channel = 0; channel < outputBuffer.getNumChannels(); ++channel)
            {
              outputBuffer.addSample(channel, startSample, wavetable[int(phase_0)] * level * adsr.getNextSample());
              phase_0 = fmod(phase_0 + increment,1080);
            }
            ++startSample;
        }
    }

private:
  double level;
  double frequency;
  int noteNumber;

  float pitchBend = 0.0f;
  float pitchBendUpSemitones = 2.0f;
  float pitchBendDownSemitones = 2.0f;

  float phase_0;
  float increment;
  double samplerate;
  float* wavetable;

  ADSR adsr;
  ADSR::Parameters adsr_parameters;
};
