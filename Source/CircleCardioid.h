/*
  ==============================================================================

    CircleCardioid.h
    Created: 30 Jun 2020 4:49:12pm
    Author:  eye

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

//==============================================================================
/*
*/
class CircleCardioid    : public Component
{

public:
    CircleCardioid()
    {
        // In your constructor, you should add any child components, and
        // initialise any special settings that your component needs.
        setSize(700, 700);

    }

    ~CircleCardioid()
    {
    }

    void paint (Graphics& g) override
    {
        /* This demo code just fills the component's background and
           draws some placeholder text to get you started.

           You should replace everything in this method with your own
           drawing code..
        */

        g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));   // clear the background

        g.setColour (Colours::grey);
        //g.drawRect (getLocalBounds(), 1);   // draw an outline around the component

        g.setColour (Colours::black);
        //g.setFont (14.0f);
        g.drawEllipse(3, 3, 666, 666, 3);


        for(int i = 0; i < points; i++)
        {
          //Calculate points on Circle
          t = MathConstants<float>::twoPi * i / points;
          x = static_cast<int> (round (3 + radius + radius * cos(t)));
          y = static_cast<int> (round (3 + radius + radius * sin(t)));

          if (points < 99){
            g.drawEllipse(x - 1, y - 1, 2, 2, 1);
          }

          t_2 = MathConstants<float>::twoPi * (i * (factor + course)) / points;
          x_2 = static_cast<int> (round (3 + radius + radius * cos(t_2) ));
          y_2 = static_cast<int>(round (3 + radius + radius * sin(t_2) ));

          g.drawLine(x, y, x_2, y_2, 2.0f);
        }

    }

    void resized() override
    {
        // This method is where you should set the bounds of any child
        // components that your component contains..

    }

private:
    int radius = 333;
    float t, t_2;
    int x, y, x_2, y_2;

    //==========================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (CircleCardioid)
    
public:
  int points = 10, factor = 2;
  float course = 0;
};
