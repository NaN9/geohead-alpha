/*
  ==============================================================================

    SynthSound.h
    Created: 6 Jul 2020 6:33:19pm
    Author:  eye

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

class SynthSound : public SynthesiserSound
{

public:
    bool appliesToNote (int midiNoteNumber) override
    {
        return true;
    }

    bool appliesToChannel (int midiNoteNumber) override
    {
        return true;
    }
};
