/*
  ==============================================================================

    FilterMenu.h
    Created: 17 Jul 2020 1:18:14pm
    Author:  eye

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"

//==============================================================================
/*
*/
class FilterMenu    : public Component
{
public:
    FilterMenu(GeoHeadalphaAudioProcessor& p) :
    processor(p)
    {
      setSize(200, 200);

      filterMenu.addItem("Low Pass", 1);
      filterMenu.addItem("High Pass", 2);
      filterMenu.addItem("Band Pass", 3);
      filterMenu.setJustificationType(Justification::centred);
      addAndMakeVisible(filterMenu);

      filterTypeAttachment = std::make_unique<AudioProcessorValueTreeState::ComboBoxAttachment>(processor.parameters, "FILTER_TYPE_ID", filterMenu);

      filterCutoff.setSliderStyle(Slider::SliderStyle::RotaryHorizontalVerticalDrag);
      filterCutoff.setRange(20.0, 10000.0);
      filterCutoff.setValue (400.0);
      filterCutoff.setTextBoxStyle(Slider::NoTextBox, false, 0, 0);
      filterCutoff.setSkewFactorFromMidPoint(1000.0);
      addAndMakeVisible(&filterCutoff);

      filterCutoffAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(processor.parameters, "CUTOFF_ID", filterCutoff);

      filterRes.setSliderStyle(Slider::SliderStyle::RotaryHorizontalVerticalDrag);
      filterRes.setRange(1, 5);
      filterRes.setValue(1);
      filterRes.setTextBoxStyle(Slider::NoTextBox, false, 0, 0);
      addAndMakeVisible(filterRes);

      filterResonanceAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(processor.parameters, "RESONANCE_ID", filterRes);


    }

    ~FilterMenu()
    {
    }

    void paint (Graphics& g) override
    {
      juce::Rectangle<int> titleArea (0, 10, getWidth(), 20);

      g.fillAll (Colours::black);
      g.setColour(Colours::white);
      g.drawText("Filter", titleArea, Justification::centredTop);

      juce::Rectangle <float> area (25, 25, 150, 150);

      g.setColour(Colours::yellow);
      g.drawRoundedRectangle(area, 20.0f, 2.0f);

    }

    void resized() override
    {
      juce::Rectangle<int> area = getLocalBounds().reduced(40);

      filterMenu.setBounds(area.removeFromTop(20));
      filterCutoff.setBounds (30, 100, 70, 70);
      filterRes.setBounds (100, 100, 70, 70);
    }

private:
    ComboBox filterMenu;
    Slider filterCutoff;
    Slider filterRes;

    //==========================================================================
    std::unique_ptr <AudioProcessorValueTreeState::ComboBoxAttachment> filterTypeAttachment;
    std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> filterCutoffAttachment;
    std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> filterResonanceAttachment;

    //==========================================================================

    GeoHeadalphaAudioProcessor& processor;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (FilterMenu)
};
