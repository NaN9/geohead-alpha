/*
  ==============================================================================

    CreateWavetable.h
    Created: 29 Jun 2020 2:15:36pm
    Author:  eye

  ==============================================================================
*/

/*
OBSOLETE
*/

#pragma once
#include <JuceHeader.h>

class CreateWavetable
{
public:
  CreateWavetable()
  {

  }
  ~CreateWavetable()
  {

  }
  //==========================================================================
  float waveform[1081] = {0};

  void postCardioidWaveVector(float position, float amplitude)
  {
       while(position > MathConstants<float>::twoPi)
       {
          position -= MathConstants<float>::twoPi;
       }

       vec_pos = static_cast<int>( round ( position * (540 / (MathConstants<float>::pi))));
       if(waveform[vec_pos] != 0.0f)
       {
         float total = (waveform[vec_pos] + amplitude) / 2;
         waveform[vec_pos] = total;
       }
       else
       {
         waveform[vec_pos] = amplitude;
       }
}

  void cardioid(std::atomic<float>* mode, std::atomic<float>* points,
                std::atomic<float>* factor, std::atomic<float>* course )
  {
    /* It calculates the distance from the origin of the circle to where the line
     is drawn. At ten points on every line. Then it calculates the average of the
     points while it's putting them in the wavetable array at the correct location
     depth is 1/3 of a degree  */
    chord_lenght = 2 * sin( (MathConstants<float>::twoPi / *points / 2));
    for (int i; i < *points; i++)
    {
      t = MathConstants<float>::twoPi * i / *points;
      t_2 = MathConstants<float>::twoPi * (i * (*factor + *course) / *points);

      radian_x = cos(t);
      radian_y = sin(t);
      radian_x_2 = cos(t_2);
      radian_y_2 = sin(t_2);

      for(float j = 0.0f; j < 1; j += 0.1)
      {
        x = j * radian_x + (1 - j) * radian_x_2;
        y = j * radian_y + (1 - j) * radian_y_2;
        rad_wav = std::sqrt(x * x + y * y);
        pos_wav = t + chord_lenght * j;

        if(*mode == 1.0f)
        {
          rad_wav = rad_wav * std::sin(pos_wav);
        }
        if(*mode == 2.0f)
        {
          if(pos_wav > MathConstants<float>::pi)
          {
            rad_wav = rad_wav * -1;
          }

        }
        if(*mode == 3.0f)
        {

        }

        postCardioidWaveVector(pos_wav, rad_wav);
      }


    }

  }
  void draw()
  {
    std::cout << "draw" << std::endl;

  }

private:
  int vec_pos;

  //============================================================================
  float t, t_2, x, y;
  float radian_x, radian_y, radian_x_2, radian_y_2;
  float rad_wav, pos_wav;
  float chord_lenght;

  //============================================================================

};
