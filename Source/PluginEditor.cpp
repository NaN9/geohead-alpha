/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"


//==============================================================================
GeoHeadalphaAudioProcessorEditor::GeoHeadalphaAudioProcessorEditor (GeoHeadalphaAudioProcessor& p)
    : AudioProcessorEditor (&p), processor (p)
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (1332, 800);

    addAndMakeVisible (modeSelektor);
    modeSelektor.addSectionHeading ("Cardioid");
    PopupMenu subMenuTravelerCard;
    modeSelektor.addItem("Sine", 1);
    modeSelektor.addItem("Square", 2);
    modeSelektor.addItem("Saw", 3);

    modeSelektor.addSectionHeading("Draw");
    modeSelektor.addItem ("Square", 4);
    modeSelektor.addItem ("Sine", 5);
    modeSelektor.addItem ("Saw", 6);


    modeSelektor.onChange = [this] { modeSelektorChanged(); };
    modeSelektor.setSelectedId (1);

    modeAttachment = std::make_unique<AudioProcessorValueTreeState::ComboBoxAttachment>(processor.parameters, "MODE_ID", modeSelektor);

    //==========================================================================

    pointSlider.setSliderStyle (Slider::SliderStyle::Rotary);
    pointSlider.setTextBoxStyle (Slider::TextBoxBelow,false, 100, 25);
    pointSlider.setRange (0.0f, 777.0f, 1.0f);
    pointSlider.setValue (10);
    pointSlider.addListener (this);
    pointSlider.setAlwaysOnTop(true);
    pointAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(processor.parameters, "POINT_ID", pointSlider);

    factorSlider.setSliderStyle (Slider::SliderStyle::Rotary);
    factorSlider.setTextBoxStyle (Slider::TextBoxBelow,false, 100, 25);
    factorSlider.setRange (0.0f, 23.0f, 1.0f);
    factorSlider.setValue (2);
    factorSlider.addListener (this);
    factorAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(processor.parameters, "FACTOR_ID", factorSlider);

    courseSlider.setSliderStyle (Slider::SliderStyle::Rotary);
    courseSlider.setTextBoxStyle (Slider::TextBoxBelow,false, 100, 25);
    courseSlider.setRange (0.000f, 1.0f, 0.003f);
    courseSlider.setValue (0.00f);
    courseSlider.addListener (this);
    courseAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(processor.parameters, "COURSE_ID", courseSlider);

    measureSlider.setSliderStyle (Slider::SliderStyle::Rotary);
    measureSlider.setTextBoxStyle (Slider::TextBoxBelow,false, 100, 25);
    measureSlider.setRange (0.000f, 127.0f, 1.0f);
    measureSlider.setValue (11.00f);
    measureSlider.addListener (this);
    measureSlider.setAlwaysOnTop(true);
    measureAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(processor.parameters, "MEASURE_ID", measureSlider);

    addAndMakeVisible (pointSlider);
    addAndMakeVisible (factorSlider);
    addAndMakeVisible (courseSlider);
    addAndMakeVisible (measureSlider);
    //==========================================================================

    attackSlider.setSliderStyle (Slider::SliderStyle::Rotary);
    attackSlider.setTextBoxStyle (Slider::TextBoxBelow,false, 100, 25);
    attackSlider.setRange (0.01f, 5.0f);
    attackSlider.setValue (0.01f);
    attackSlider.addListener (this);
    attackSlider.setAlwaysOnTop(true);
    attackAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(processor.parameters, "ATTACK_ID", attackSlider);

    decaySlider.setSliderStyle (Slider::SliderStyle::Rotary);
    decaySlider.setTextBoxStyle (Slider::TextBoxBelow,false, 100, 25);
    decaySlider.setRange (0.01f, 2.0f);
    decaySlider.setValue (0.01f);
    decaySlider.addListener (this);
    decaySlider.setAlwaysOnTop(true);
    decayAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(processor.parameters, "DECAY_ID", decaySlider);

    sustainSlider.setSliderStyle (Slider::SliderStyle::Rotary);
    sustainSlider.setTextBoxStyle (Slider::TextBoxBelow,false, 100, 25);
    sustainSlider.setRange (0.0f, 1.0f);
    sustainSlider.setValue (1.0f);
    sustainSlider.addListener (this);
    sustainSlider.setAlwaysOnTop(true);
    sustainAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(processor.parameters, "SUSTAIN_ID", sustainSlider);

    releaseSlider.setSliderStyle (Slider::SliderStyle::Rotary);
    releaseSlider.setTextBoxStyle (Slider::TextBoxBelow,false, 100, 25);
    releaseSlider.setRange (0.01f, 5.0f);
    releaseSlider.setValue (0.01f);
    releaseSlider.addListener (this);
    releaseSlider.setAlwaysOnTop(true);
    releaseAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(processor.parameters, "RELEASE_ID", releaseSlider);

    addAndMakeVisible (attackSlider);
    addAndMakeVisible (decaySlider);
    addAndMakeVisible (sustainSlider);
    addAndMakeVisible (releaseSlider);

    //==========================================================================
    button_left.addListener(this);
    button_right.addListener(this);
    button_left.setClickingTogglesState (true);
    button_right.setClickingTogglesState (true);

    addAndMakeVisible(button_left);
    addAndMakeVisible(button_right);

    buttonLAttachment = std::make_unique<AudioProcessorValueTreeState::ButtonAttachment>(processor.parameters, "BUTTON_L_ID", button_left);
    buttonRAttachment = std::make_unique<AudioProcessorValueTreeState::ButtonAttachment>(processor.parameters, "BUTTON_R_ID", button_right);

    //==========================================================================
    addAndMakeVisible (cardioidComponent);
    addAndMakeVisible (wavetableComponent);
    addAndMakeVisible (filterComponent);

    //==========================================================================


}

GeoHeadalphaAudioProcessorEditor::~GeoHeadalphaAudioProcessorEditor()
{
}

//==============================================================================
void GeoHeadalphaAudioProcessorEditor::paint (Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));

    g.setColour (Colours::white);
    g.setFont (15.0f);

    switch(algorithm)
    {
      case 1: g.drawText 	( "Dutty 0", 1110, 300, 100, 20, Justification::centredLeft);
              break;
      case 2: g.drawText 	( "Dutty 1", 1110, 300, 100, 20, Justification::centredLeft);
              break;
      case 3: g.drawText 	( "Stepper", 1110, 300, 100, 20, Justification::centredLeft);
              break;
    }

}

void GeoHeadalphaAudioProcessorEditor::modeSelektorChanged()
{
  mode = modeSelektor.getSelectedId();
  repaint();
  copyWaveform();
}

void GeoHeadalphaAudioProcessorEditor::sliderValueChanged(Slider *slider)
{
  if(slider == &pointSlider)
  {
    cardioidComponent.points = static_cast<int>(pointSlider.getValue());
  }
  if(slider == &factorSlider)
  {
    cardioidComponent.factor = static_cast<int>(factorSlider.getValue());
  }

  if(slider == &courseSlider)
  {
    cardioidComponent.course = courseSlider.getValue();
  }
  copyWaveform();
  repaint();
}

void GeoHeadalphaAudioProcessorEditor::buttonClicked(Button *button)
{
  if(button == &button_left)
  {
    if(algorithm < 2)
    {
      algorithm = 3;
    } else
    {
      algorithm--;
    }
  }
  if(button == &button_right)
  {
    if(algorithm > 2)
    {
      algorithm = 1;
    } else
    {
      algorithm++;
    }
  }
  copyWaveform();
  repaint();
}

void GeoHeadalphaAudioProcessorEditor::copyWaveform()
{
    while (processor.waveform_holder[270] != 0 && processor.waveform_holder[810] != 0 && processor.waveform_holder[1070] != 0 ) {
      //pause until waveform holder is overwritten with 0
    }
    for(int i = 0; i < 1080; i++)
    {
      wavetableComponent.waveform[i] = processor.waveform[i];
      //std::cout << processor.waveform[i] << std::endl;
    }
}

void GeoHeadalphaAudioProcessorEditor::resized()
{
  modeSelektor.setBounds (getWidth()/2 - 50, 33, 100, 20);

  pointSlider.setBounds(33, 64, 100, 100);
  factorSlider.setBounds(243, 3, 100, 100);
  courseSlider.setBounds(343, 3, 100, 100);
  measureSlider.setBounds(553, 64, 100, 100);

  cardioidComponent.setBounds(10, 100, 700, 700);

  button_left.setBounds(1053, 300, 40, 20);
  button_right.setBounds(1253, 300, 40, 20);
  wavetableComponent.setBounds(732, 326, 561, 214);

  attackSlider.setBounds(733, 550, 100, 100);
  decaySlider.setBounds(833, 550, 100, 100);
  sustainSlider.setBounds(933, 550, 100, 100);
  releaseSlider.setBounds(1033, 550, 100, 100);

  filterComponent.setBounds(780, 50, 200, 200);

}
